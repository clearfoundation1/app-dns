<?php

$lang['dns_alias'] = 'Псевдонім';
$lang['dns_app_description'] = 'Локальний DNS-сервер можна використовувати для співставлення IP-адрес у вашій мережі з іменами хостів.';
$lang['dns_app_name'] = 'DNS-сервер';
$lang['dns_dns_entry'] = 'Запис DNS';
$lang['dns_dns_entries'] = 'Записи DNS';
$lang['dns_dns_server'] = 'DNS-сервер';
$lang['dns_active_directory_domains'] = 'Домени Active Directory';
$lang['dns_primary_dns'] = 'Первинний DNS';
$lang['dns_secondary_dns'] = 'Вторинний DNS';
